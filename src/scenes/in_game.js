function InGame() {
    var self = this;
    self.container = new PIXI.Container();
    self.hud = new PIXI.Container();
    self.space = new PIXI.Container();
    
    self.container.addChild(self.space);
    self.container.addChild(self.hud);
    
    self.objects = {};
    
    // Shield and Armor Bars
    var bar_length = screen_width / 3;
    var bar_height = 15;
    var bar_rounded = 7;
    
    var shield_bar_x = 25;
    var shield_bar_y = 25;
    self.shield_bar = new StatusBar(
        shield_bar_x,
        shield_bar_y,
        bar_length,
        bar_height,
        bar_rounded
    );
    self.shield_bar.filled_color = 0x22bbff;
    self.shield_bar.draw();
    self.hud.addChild(self.shield_bar.get_sprite());
    
    var armor_bar_x = 25;
    var armor_bar_y = shield_bar_y + 25;
    self.armor_bar = new StatusBar(
        armor_bar_x,
        armor_bar_y,
        bar_length,
        bar_height,
        bar_rounded
    );
    self.armor_bar.filled_color = 0x555555;
    self.armor_bar.draw();
    self.hud.addChild(self.armor_bar.get_sprite());
    
    self.add_object = function(object) {
        self.objects[object.id] = object;
        self.space.addChild(object.get_sprite());
    };
    
    self.update_object = function(message) {
        var object_id = message['id'];
        if (self.objects[object_id]) {
            self.objects[object_id].x = message['x'];
            self.objects[object_id].y = message['y'];
            self.objects[object_id].d = message['d'];
            self.objects[object_id].x_vel = message['x_vel'];
            self.objects[object_id].y_vel = message['y_vel'];
            self.objects[object_id].d_vel = message['d_vel'];
        }
    };
    
    self.remove_object = function(message) {
        var object_id = message['id'];
        var object = self.objects[object_id];
        self.space.removeChild(object.get_sprite());
        delete self.objects[object_id];
    };
    
    self.update = function(delta_time) {
        self.shield_bar.draw();
        self.armor_bar.draw();
        
        for (object_id in self.objects) {
            var object = self.objects[object_id];
            object.update(delta_time);
        }
    };
};
