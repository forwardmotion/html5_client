/* jshint browser:true */
/* globals PIXI, requestAnimationFrame */
(function() {
    
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    
    document.addEventListener('DOMContentLoaded', function() {
        player_id = -1;
        // Message Handler
        var handle_message = {
            [NEW_ENTITY]: function(message) {
                var image = IMAGES[message['image']];
                var new_entity = new GenericObject(image.path, image.scale);
                // var new_entity = new GenericObject('asset/bunny.png', [1,1]);
                new_entity.id = message['id'];
                new_entity.x = message['x'];
                new_entity.y = message['y'];
                new_entity.d = message['d'];
                new_entity.x_vel = message['x_vel'];
                new_entity.y_vel = message['y_vel'];
                new_entity.d_vel = message['d_vel'];
                in_game.add_object(new_entity);
            },
            [UPDATE_ENTITY]: function(message) {
                in_game.update_object(message);
            },
            [LOST_ENTITY]: function(message) {
                in_game.remove_object(message);
            },
            [PLAYER_INFO]: function(message) {
                in_game.shield_bar.max = message['max_shield'];
                in_game.shield_bar.current = message['cur_shield'];
                in_game.armor_bar.max = message['max_armor'];
                in_game.armor_bar.current = message['cur_armor'];
                player_id = message['id'];
            }
        };
        
        
        // Controls
        function update_controls(controls) {
            var message = create_message(UPDATE_CONTROLS);
            message['thrust'] = controls['thrust'];
            message['direction'] = controls['direction'];
            message['weapon'] = controls['weapon'];
            network.send_message(message);
        };
        
        var controls = {
            'thrust': 0,
            'direction': 0,
            'weapon': 0,
        }
        
        var key_w = keyboard(87); // Up
        key_w.press = function() {
            controls['thrust'] = 1;
            update_controls(controls);
        };
        key_w.release = function() {
            controls['thrust'] = 0;
            update_controls(controls);
        };
        
        var key_a = keyboard(65); // Left
        key_a.press = function() {
            controls['direction'] = 1;
            update_controls(controls);
        };
        key_a.release = function() {
            if (key_d.isDown) {
                controls['direction'] = 2;
            } else {
                controls['direction'] = 0;
            }
            update_controls(controls);
        };
        
        var key_d = keyboard(68); // Right
        key_d.press = function() {
            controls['direction'] = 2;
            update_controls(controls);
        };
        key_d.release = function() {
            if (key_a.isDown) {
                controls['direction'] = 1;
            } else {
                controls['direction'] = 0;
            }
            update_controls(controls);
        };
        
        var key_space = keyboard(32);
        key_space.press = function() {
            controls['weapon'] = 1;
            update_controls(controls);
        };
        key_space.release = function() {
            controls['weapon'] = 0;
            update_controls(controls);
        };
        
        // Connect to server
        var network = new Network();
        
        // create a renderer instance
        // var width = screen.availWidth;
        // var height = screen.availHeight;
        screen_width = window.innerWidth;
        screen_height = window.innerHeight;
        var renderer = new PIXI.autoDetectRenderer(screen_width, screen_height, {backgroundColor : 0x000000});
        
        camera_x = screen_width / 2;
        camera_y = screen_height / 2;

        // add the renderer view element to the DOM
        document.body.appendChild(renderer.view);

        // create an new instance of a pixi stage
        var stage = new PIXI.Container();
        
        // Scenes
        var in_game = new InGame();
        var current_scene = null;
        
        test = in_game;
        
        // Set the Current Scene
        current_scene = in_game.container;
        
        // create test sprites
        // for (var i = 0; i < 10; i++) {
        //     var bunny = new GenericObject("asset/bunny.png");
        //     // Move the sprite to a random position on the screen
        //     bunny.x = getRandomInt(0, width);
        //     bunny.y = getRandomInt(0, height);
        //     bunny.d_vel = getRandomInt(0, 90);
        //     
        //     in_game.add_object(bunny);
        // }
        
        requestAnimationFrame(run);
        
        var ticker = PIXI.ticker.shared;
        ticker.autoStart = false;
        ticker.stop();
        var last_update = window.performance.now();
        var delta_time = 0;
        function run(timestamp) {
            // timestamp /= 1000;
            timestamp = window.performance.now();
            delta_time = (timestamp - last_update) / 1000;
            last_update = timestamp;
            
            var messages = network.get_messages();
            for (message of messages) {
                var message_id = message['message_id'];
                if (handle_message[message_id]) {
                    handle_message[message_id](message);
                }
            }
            
            in_game.update(delta_time);

            // render the stage
            if (current_scene) {
                renderer.render(current_scene);
            }
            
            last_update = timestamp;
            ticker.update(timestamp);
            setTimeout(function() {
                requestAnimationFrame(run);
            }, 1000 / 30);
        }
    }, false);

}());
