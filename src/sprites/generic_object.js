function GenericObject(texture_path, scale) {
    var self = this;
    var texture = PIXI.Texture.fromImage(texture_path);
    self.sprite = new PIXI.Sprite(texture);
    self.sprite.anchor.x = 0.5;
    self.sprite.anchor.y = 0.5;
    if (scale) {
        self.sprite.scale.x /= scale[0];
        self.sprite.scale.y /= scale[1];
    }
    
    self.id = 0;
    
    self.x = 0;
    self.y = 0;
    self.d = 0;
    
    self.x_vel = 0;
    self.y_vel = 0;
    self.d_vel = 0;
    
    self.is_camera = false;
    
    var center_x = screen_width / 2;
    var center_y = screen_height / 2;
    
    self.update = function(delta_time) {
        self.x += self.x_vel * delta_time;
        self.y += self.y_vel * delta_time;
        self.d += self.d_vel * delta_time;
        self.d = self.d % 360;
        
        // if (self.is_camera) {
        if (player_id == self.id) {
            camera_x = self.x;
            camera_y = self.y;
            self.sprite.position.x = center_x;
            self.sprite.position.y = center_y;
        } else {
            // Y is negative because 0,0 starts on the top left instead of bottom left.
            self.sprite.position.x = self.x - camera_x + center_x;
            self.sprite.position.y = -self.y + camera_y + center_y;
        }
        self.sprite.rotation = Math.radians(self.d);
    };
    
    self.get_sprite = function() {
        return self.sprite;
    };
};

Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};
