function StatusBar(x, y, width, height, rounded) {
    // Shield and Armor Bars
    var self = this;
    self.bar_length = width;
    self.bar_height = height;
    self.bar_rounded = rounded;
    
    self.status_bar_x = x;
    self.status_bar_y = y;
    
    self.filled_color = 0x0055ff;
    self.unfilled_color = 0xaaaaaa;
    
    self.max = 100;
    self.current = 50;
    
    self.status_bar = new PIXI.Graphics();
    self.status_bar.alpha = 0.7;
    
    self.draw = function() {
        if (self.current <= 0) {
            var final_fill = self.unfilled_color;
            self.current = 0;
        } else {
            var final_fill = self.filled_color;
        }
        self.status_bar.clear();
        self.status_bar.beginFill(self.unfilled_color);
        self.status_bar.drawRoundedRect(
            self.status_bar_x,
            self.status_bar_y,
            self.bar_length,
            self.bar_height,
            self.bar_rounded
        );
        
        self.status_bar.endFill();
        self.status_bar.beginFill(final_fill);
        self.status_bar.drawRoundedRect(
            self.status_bar_x,
            self.status_bar_y,
            self.bar_length * (self.current / self.max),
            self.bar_height,
            self.bar_rounded
        );
        self.status_bar.endFill();
    }
    
    self.get_sprite = function() {
        return self.status_bar;
    };
};
