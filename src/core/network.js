// Let us open a web socket
function Network() {
    direct_access = this;
    var self = this;
    
    self.queue = [];
    // self.ws = new WebSocket("ws://168.235.145.95:22100/");
    // self.ws = new WebSocket("ws://45.62.249.78:22100/");
    self.ws = new WebSocket("ws://127.0.0.1:22100/");
    self.send_message = function(message) {
        self.ws.send(JSON.stringify(message));
    };
    
    self.get_messages = function() {
        var temp_queue = self.queue.splice(0);
        self.queue = [];
        return temp_queue;
    }
    
    self.ws.onopen = function() {
        // Web Socket is connected, send data using send()
        self.send_message({'test': 'Hello World!'});
        console.log("Connected!");
    };
    
    self.ws.onmessage = function (evt) {
        if (evt.data == 'pong') {
            console.log('pong');
            return;
        }
        
        var received_msg = JSON.parse(evt.data);
        self.queue.push(received_msg);
    };
    
    self.ws.onclose = function() { 
        // websocket is closed.
        alert("Connection is closed..."); 
    };
    
}
