IMAGES = {
    [1]: {
        path: 'asset/images/shuttle_overhead.png',
        scale: [15, 15],
    },
    [2]: {
        path: 'asset/images/hero_bullet.png',
        // scale: [1, 1],
    },
    [3]: {
        path: 'asset/images/earth.png'
    },
    [4]: {
        path: 'asset/images/hero.png'
    },
    [5]: {
        path: 'asset/images/enemy.png'
    },
    [6]: {
        path: 'asset/images/enemy_bullet.png'
    },
    [7]: {
        path: 'asset/images/mars.png'
    },
};
