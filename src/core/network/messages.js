function create_message(message_id) {
    var message = clone(messages[message_id]);
    message['timestamp'] = Date.now();
    message['message_id'] = message_id;
    return message;
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
    }
    return copy;
}

messages = {
    [LOST_CONNECTION]: {
        'reason': '',
    },
    [SEND_MESSAGE]: {
        'target': 0,
        'type': 0,
        'message': '',
    },
    [RECV_MESSAGE]: {
        'sender': 0,
        'type': 0,
        'message': '',
    },
    [UPDATE_CONTROLS]: {
        'thrust': 0,
        'direction`': 0,
        'weapon': 0,
    },
    [PLAYER_INFO]: {
        'id': 0,
        'name': '',
        'max_shield': 0,
        'max_armor': 0,
        'max_fuel': 0,
        'cur_shield': 0,
        'cur_armor': 0,
        'cur_fuel': 0,
    },
    [UPDATE_ENTITY]: {
        'id': 0,
        'x': 0,
        'y': 0,
        'd': 0,
        'x_vel': 0,
        'y_vel': 0,
        'd_vel': 0,
    },
    [NEW_ENTITY]: {
        'id': 0,
        'type': 0,
        'image': 0,
        'poly_points': 0,
        'x': 0,
        'y': 0,
        'd': 0,
        'x_vel': 0,
        'y_vel': 0,
        'd_vel': 0,
    },
    [LOST_ENTITY]: {
        'id': 0,
        'explosion_type': 0,
        'explosion_size': 0,
    },
    [PLAYER_JOINED]: {},
    [CLEAR_ENTITIES]: {},
};
