// 51 - 99: Coordinator Message IDs (Coordinator > Node|Service)
NEW_CONNECTION = 51 // Tell the TC there was a new connection.
MOVE_CONNECTION = 52 // Tell the LB to move the user to another server.
LOST_CONNECTION = 53 // Tell the TC a client has disconnected.
NEW_NODE = 54 // Tell the LB a node has been connected.
REMOVE_NODE = 55 // Tell the LB a node has been removed.
WHO_ARE_YOU = 56 // Ask a Node who they are.
PROXY_ID = 57 // Update tbe Coordinator with the server connection ID. 

// 101 - 149: Node Message IDs (Node > Coordinator|Service)
JUMP_REQUEST = 101 // There is a jump request from the node.
JUMP_ACCEPTED = 102 // Respond that the jump request has been approved.
JUMP_DENIED = 103 // Respond that the jump request has been denied.
UPDATE_CLIENT_INFO = 104 // Update the client information.
SAVE_CLIENT = 105 // Save the client data.

// 151 - 199: Client Message IDs (Client > Node)
SEND_MESSAGE = 151
RECV_MESSAGE = 152
UPDATE_CONTROLS = 153
PLAYER_INFO = 154

NEW_ENTITY = 155
LOST_ENTITY = 156
UPDATE_ENTITY = 157
CREATE_EXPLOSION = 158
CLEAR_ENTITIES = 159

PLAYER_JOINED = 160

// 200 - 255: Basic Authentication and Administration
LOGIN = 200
REGISTER = 201


// CONSTANTS
PRIMARY_WEAPON = 1
SECONDARY_WEAPON = 2

// Dictionary keys
TIMESTAMP = 1
MESSAGE_ID = 2
